require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: "Example User", email: "user@example.com", password: "plop123", password_confirmation: "plop123")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "blank name should be invalid" do
    @user.name = "       "
    assert_not @user.valid?
  end

  test "blank email should be invalid" do
    @user.email = "       "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "longest name should be valid" do
    @user.name = "a" * 50
    assert @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

  test "longest email should be valid" do
    @user.email = "a" * 243 + "@example.com"
    assert @user.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved lowercase" do
    user = User.create(name: "Plop", email: "PLOP@gmail.com", password: "plop123", password_confirmation: "plop123")
    assert_equal  "plop@gmail.com" , user.email
  end

  test "empty user should be invalid" do
    user = User.new
    assert_not user.valid?
  end

  test "user without password should be invalid" do
    user = User.new(name: "Plop", email: "PLOP@gmail.com")
    assert_not user.valid?
  end

  test "user without matching confirmation password should be invalid" do
    user = User.new(name: "Plop", email: "PLOP@gmail.com", password: "plop", password_confirmation: "abcd")
    assert_not user.valid?
  end

  test "user with short password should be invalid" do
    user = User.new(name: "Plop", email: "PLOP@gmail.com", password: "plop1", password_confirmation: "plop1")
    assert_not user.valid?
  end

  test "user with blank password should be invalid" do
    user = User.new(name: "Plop", email: "PLOP@gmail.com", password: "      ", password_confirmation: "      ")
    assert_not user.valid?
  end

  test "user with min length password should be valid" do
    user = User.new(name: "Plop", email: "PLOP@gmail.com", password: "plop12", password_confirmation: "plop12")
    assert user.valid?
  end

  test "token digest matched should return false for a user with nil digest" do
    assert_not @user.is_valid_token?('remember', '')
  end

  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end

  test "should follow and unfollow a user" do
    sampleuser = users(:sampleuser)
    archer  = users(:archer)
    assert_not sampleuser.following?(archer)
    sampleuser.follow(archer)
    assert sampleuser.following?(archer)
    assert archer.followers.include?(sampleuser)
    sampleuser.unfollow(archer)
    assert_not sampleuser.following?(archer)
  end

  test "feed should have the right posts" do
    user = users(:sampleuser)
    archer  = users(:archer)
    lana    = users(:lana)
    # Posts from followed user
    lana.microposts.each do |post_following|
      assert user.feed.include?(post_following)
    end
    # Posts from self
    user.microposts.each do |post_self|
      assert user.feed.include?(post_self)
    end
    # Posts from unfollowed user
    archer.microposts.each do |post_unfollowed|
      assert_not user.feed.include?(post_unfollowed)
    end
  end
end