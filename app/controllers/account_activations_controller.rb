class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(id: params[:key])
    if user && !user.activated? && user.is_valid_token?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = "Account activated!"
      redirect_to user
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end
end
